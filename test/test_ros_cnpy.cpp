/*
 * MIT License
 *
 * Copyright (c) 2020 Mapless AI, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <gtest/gtest.h>

#include <complex>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

#include "ros_cnpy/cnpy.h"

static constexpr int Nx = 128;
static constexpr int Ny = 64;
static constexpr int Nz = 32;

//------------------------------------------------------------------------------

TEST(ROS_cnpy, tests) {
    // filenames for testing
    char arr1_filename[] = "arr1_XXXXXX.npy";
    //const auto fd_arr1 = mkstemps(arr1_filename, strlen(".npy"));
    //ASSERT_NE(fd_arr1, -1) << "Failed to create temp file: " << arr1_filename;
    char out_filename[] = "out_XXXXXX.npz";
    //const auto fd_out = mkstemps(out_filename, strlen(".npz"));
    //ASSERT_NE(fd_out, -1) << "Failed to create temp file: " << out_filename;

    //set random seed so that result is reproducible (for testing)
    srand(0);
    //create random data
    std::vector<std::complex<double>> data(Nx*Ny*Nz);
    for(int i = 0;i < Nx*Ny*Nz;i++) data[i] = std::complex<double>(rand(),rand());

    //save it to file
    cnpy::npy_save(arr1_filename,&data[0],{Nz,Ny,Nx},"w");

    //load it into a new array
    cnpy::NpyArray arr = cnpy::npy_load(arr1_filename);
    std::complex<double>* loaded_data = arr.data<std::complex<double>>();

    //make sure the loaded data matches the saved data
    ASSERT_EQ(arr.word_size, sizeof(std::complex<double>));
    ASSERT_TRUE(arr.shape.size() == 3 && arr.shape[0] == Nz && arr.shape[1] == Ny && arr.shape[2] == Nx);
    for(int i = 0; i < Nx*Ny*Nz;i++) ASSERT_TRUE(data[i] == loaded_data[i]);

    //append the same data to file
    //npy array on file now has shape (Nz+Nz,Ny,Nx)
    cnpy::npy_save(arr1_filename,&data[0],{Nz,Ny,Nx},"a");

    //now write to an npz file
    //non-array variables are treated as 1D arrays with 1 element
    double myVar1 = 1.2;
    char myVar2 = 'a';
    cnpy::npz_save(out_filename,"myVar1",&myVar1,{1},"w"); //"w" overwrites any existing file
    cnpy::npz_save(out_filename,"myVar2",&myVar2,{1},"a"); //"a" appends to the file we created above
    cnpy::npz_save(out_filename,"arr1",&data[0],{Nz,Ny,Nx},"a"); //"a" appends to the file we created above

    //load a single var from the npz file
    cnpy::NpyArray arr2 = cnpy::npz_load(out_filename,"arr1");

    //load the entire npz file
    cnpy::npz_t my_npz = cnpy::npz_load(out_filename);

    //check that the loaded myVar1 matches myVar1
    cnpy::NpyArray arr_mv1 = my_npz["myVar1"];
    double* mv1 = arr_mv1.data<double>();
    ASSERT_TRUE(arr_mv1.shape.size() == 1 && arr_mv1.shape[0] == 1);
    ASSERT_TRUE(mv1[0] == myVar1);

    const auto rm_arr1 = remove(arr1_filename);
    const auto rm_out = remove(out_filename);
    EXPECT_EQ(rm_arr1, 0) << "Failed to remove temp file: " << arr1_filename;
    EXPECT_EQ(rm_out, 0) << "Failed to remove temp file: " << out_filename;
}

//------------------------------------------------------------------------------

