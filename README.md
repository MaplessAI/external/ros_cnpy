# NOTE:

**As of 2020-07-24 this package is deprecated in favor of the ROS 2 version: [ros\_2\_cnpy](https://gitlab.com/MaplessAI/external/ros_2_cnpy)**

This code is just a ROS version of the [cnpy](https://github.com/rogersce/cnpy) library as of commit [4e8810b](https://github.com/rogersce/cnpy/tree/4e8810b1a8637695171ed346ce68f6984e585ef4).

The original library has been slightly modified to replace `assert` statements with exceptions; otherwise, no major modifications have been made.

The original `example1.cpp` file has been slightly modified to convert it into a unit test file.

Thanks and credit to [Carl Rogers](https://github.com/rogersce) for providing the library.

# Purpose:

NumPy offers the `save` method for easy saving of arrays into .npy and `savez` for zipping multiple .npy arrays together into a .npz file. 

`cnpy` lets you read and write to these formats in C++. 

The motivation comes from scientific programming where large amounts of data are generated in C++ and analyzed in Python.

Writing to .npy has the advantage of using low-level C++ I/O (fread and fwrite) for speed and binary format for size. 
The .npy file header takes care of specifying the size, shape, and data type of the array, so specifying the format of the data is unnecessary.

Loading data written in numpy formats into C++ is equally simple, but requires you to type-cast the loaded data to the type of your choice.

# Installation and usage:

This is a standard ROS package. Install dependencies with `rosdep` and build as normal.

Depend on this package in your own ROS packages and `#include "ros_cnpy/cnpy.h"` in your source code to use the library.

# Description:

There are two functions for writing data: `npy_save` and `npz_save`.

There are 3 functions for reading:
- `npy_load` will load a .npy file. 
- `npz_load(fname)` will load a .npz and return a dictionary of NpyArray structues. 
- `npz_load(fname,varname)` will load and return the NpyArray for data varname from the specified .npz file.

The data structure for loaded data is below. 
Data is accessed via the `data<T>()`-method, which returns a pointer of the specified type (which must match the underlying datatype of the data). 
The array shape and word size are read from the npy header.

```c++
struct NpyArray {
    std::vector<size_t> shape;
    size_t word_size;
    template<typename T> T* data();
};
```

# Unit tests

[test/test\_ros\_cnpy.cpp](test/test_ros_cnpy.cpp) contains tests and examples of how to use the library.

`catkin_make run_tests_ros_cnpy` to build and run the tests.
